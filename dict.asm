
%define PTR_TO_PREV_SIZE 8

extern string_equals
section .text

global find_word

;rdi - указатель на нуль-терминированную 
;rsi - указатель на начало словаря

find_word:
    .loop:
    	test rsi, rsi
    	jz .end
    	push rsi
    	add rsi, PTR_TO_PREV_SIZE
    	call string_equals
    	pop rsi
    	test rax, rax
    	jne .end
    	mov rsi, [rsi]
    	jmp .loop
    	
    .end:
    	mov rax, rsi
    	ret
    
