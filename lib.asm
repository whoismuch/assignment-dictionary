section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global print_error
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
 
%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDERR_DESCRIPTOR 2
%define STDOUT_DESCRIPTOR 1
%define STDIN_DESCRIPTOR 0

%define SYMB_TAB 0x9

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
    	cmp byte [rdi+rax], 0 
    	je .end
    	inc rax
    	jmp .count
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rsi, rdi
    mov rdi, STDOUT_DESCRIPTOR
    syscall
    ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stderr

print_error:
   call string_length
   mov rdx, rax
   mov rax, SYS_WRITE
   mov rsi, rdi
   mov rdi, STDERR_DESCRIPTOR
   syscall
   ret 
   
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0xA
    
; Принимает код символа и выводит его в stdout
print_char:
    mov rax, SYS_WRITE 
    dec rsp
    mov byte [rsp], dil 
    mov rsi, rsp
    mov rdi, STDOUT_DESCRIPTOR
    mov rdx, 1
    syscall
    inc rsp
    ret


    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rax, rdi
    xor rdx, rdx
    xor rcx, rcx
    dec rsp
    mov byte [rsp], 0
    .loop:
	mov rbx, 10
	div rbx
	dec rsp
	add dl, '0'
	mov byte [rsp], dl
	xor rdx, rdx
	inc cx
	test rax, rax
	jne .loop
    .end: 
    	mov rdi, rsp
    	push rcx
    	call print_string
    	pop rcx
    	add rsp, rcx 
    	inc rsp 
    	pop rbx
    	ret
    	
    	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:    
    test rdi, rdi
    jns .begin
    
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    
    .begin: 
    	call print_uint
    	ret

   

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

;rdi - pointer to string 1
;rsi - pointer to string 2

string_equals:
    push rbx
    xor rdx, rdx
    .loop:
    	mov bl, byte[rdi+rdx]
    	mov cl, byte[rsi+rdx]
    	cmp bl, cl
    	jne .notequal
    	cmp bl, 0
    	je .equal
    	inc rdx
    	jmp .loop
    .notequal:
    	xor rax, rax
    	jmp .end
    .equal:
    	mov rax, 1
    	jmp .end
    .end:
    	pop rbx
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov [rsp], byte 0
    mov rdi, STDIN_DESCRIPTOR
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_READ
    syscall
    mov al, byte[rsp]
    inc rsp
    ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

;rdi - address of buffer beginning
;rsi - the size of buffer

read_word:
 xor rdx, rdx
    .loop:
        cmp rdx, rsi
        je .fail
        
    	push rdx
    	push rdi
    	push rsi
    	
        call read_char 
        
        pop rsi
        pop rdi
        pop rdx
       
        cmp rax, ' '
        je .succ
        cmp rax, SYMB_TAB
        je .succ
        cmp rax, 0xA
        je .succ
        cmp rax, 0
        je .ex
        
        mov byte[rdi+rdx], al
        
        inc rdx  
        jmp .loop
        
    .fail: 
    	xor rax, rax
    	ret
    .succ:
    	test rdx, rdx
    	je .loop
    .ex:
    	mov rax, rdi
    	mov byte[rdi+rdx], 0
    	ret
     
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к строке нуль-терминатор
      
read_line:
xor rdx, rdx
    .loop:
        cmp rdx, rsi
        je .longlength
        
    	push rdx
    	push rdi
    	push rsi
    	
        call read_char 
        
        pop rsi
        pop rdi
        pop rdx
       
        cmp rax, 0xA
        je .ex
          
    	 mov byte[rdi+rdx], al
        inc rdx  
        jmp .loop
        
    .ex:
    	mov rax, rdi
    	mov byte[rdi+rdx], 0
    	ret
    	
    .longlength:
    	xor rax, rax
    	ret
        

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

; rdi - address of buffer beginning

parse_uint:
    push rbx
    xor rdx, rdx
    xor rax, rax
    xor rbx, rbx
    xor rcx, rcx
    .loop:
        mov cl, byte[rdi+rdx] 
        sub cl, '0'
        jl .end 
        cmp cl, 9
        jg .end
        
        push rdx
        mov rbx, 10
        mul rbx
        pop rdx
        
        add rax, rcx
        
        inc rdx
        jmp .loop
    .end: 
    	pop rbx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx
    mov bl, byte[rdi]
    cmp bl, '-'
    je .negative
    .positive:
    	call parse_uint
    	jmp .end
    .negative:
   	inc rdi
    	call parse_uint
        neg rax
        dec rdi
        inc rdx
    .end:
    	pop rbx
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rbx
    xor rax, rax
    .loop:
    	cmp rax, rdx 
    	je .notfit
    	mov bl, [rdi + rax]
    	mov byte[rsi + rax], bl
    	cmp byte[rdi + rax], 0
    	je .fit
    	inc rax
    	jmp .loop
    .notfit:
    	xor rax, rax
    .fit:
    	pop rbx
    	ret
    
