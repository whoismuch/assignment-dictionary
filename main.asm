%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUF_SIZE 255
%define BYTE_SIZE 8

global _start

extern find_word

section .rodata
longlength:	db "Введенный вами ключ превышает допустимую норму в 255 символов", 0
cant_find_msg: db "По заданному вами ключу слова нет", 0

section .text

_start:
	sub rsp, BUF_SIZE
	mov rsi, BUF_SIZE
	mov rdi, rsp
	call read_line

	test rax, rax
	jz .len_err
	
	mov rsi, prev
	mov rdi, rax
	call find_word ;rax = address of entering in dictionary
	
	test rax, rax
	jz .cant_find
	mov rdi, rax
	add rdi, BYTE_SIZE
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi, rax

	call print_string
	jmp .end
	
.len_err:
	mov rdi, longlength
	call print_error
	jmp .end
	
.cant_find:
	mov rdi, cant_find_msg
	call print_error
.end:
	add rsp, BUF_SIZE
	call print_newline
	call exit	







